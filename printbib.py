#!/usr/bin/python3

import sys,os,string

usage_str = """Usage:

Print all entries in the bibtex file, formatted consistently and sorted by year-->author:
  printbib.py filename.bib

Print just certain entries, e.g. those with entry names brier1950 and good1952:
  printbib.py filename.bib brier1950 good1952

Additionally re-name each entry to a shortened version of authorYYYYtitle:
  printbib.py filename.bib --rename

Print sorted alphabetically by author (rather than year):
  printbib.py filename.bib --author

Print a plain text, "citation" version of each entry rather than a bibtex entry:
  printbib.py filename.bib --plain

e.g. print plain text versions of two entries sorted by author:
  printbib.py filename.bib --plain --author brier1950 good1952



Expects the following .bib file format (whitespace within a line irrelevant):

@entrytype{entryname,
  field1 = {content1},
  field2 = {content2},
  ...
}

Authors MUST be separated by " and ".

"""

# when re-formatting, print the fields of an entry in this order
# (for those fields that are present)
preferred_fields_order = ["title", "author", "year", "journal", "chapter", "booktitle", "series", "volume", "number", "pages", "publisher"]

punct = set(string.punctuation)
def simplify(s):
  return "".join(c for c in s.lower() if c not in punct)

# skip these words at the beginning of a title
title_skipset = set(['a', 'an', 'are', 'from', 'on', 'the', 'what', 'when', 'you'])

# return a function of an entry that gives the string to sort it with
# use entry name as a backup to break ties
def get_sort_key(sort_by):
  def sort_key(e):
    if sort_by not in e[2]:
      return ("0","0","0")
    return (e[2][sort_by], e[1])
  return sort_key


class Bib:
  def __init__(self):
    # a list of triples (entrytype, entryname, dict)
    # where an entry in the bib file looks like:
    # @entrytype{entryname,
    #   dict_key_1: {dict_value_1},
    #   ...
    # }
    self.entries = []

  def readfile(self, filename, entrylist):
    self.entries = get_refs(filename, entrylist)

  def is_empty(self):
    return len(self.entries) == 0

  def get_bib_lines(self, rename=False, sort_by="year"):
    self.entries.sort(key = get_sort_key(sort_by))
    if rename:
      for e in self.entries:
        e[1] = format_entry_name(e)
    lines = []
    for e in self.entries:
      append_bib_entry(e, lines)
      lines.append("")
    return lines

  def get_plain_lines(self, sort_by="year"):
    self.entries.sort(key = get_sort_key(sort_by))
    lines = []
    for e in self.entries:
      lines.append(get_plain_entry(e, sort_by == "year"))
    return lines

def format_author(auth):
  authors = auth.split(" and ")
  def rearrange(a):
    if "," in a:
      last = a[:a.rfind(",")].strip()
      first = a[a.rfind(",")+1:].strip()
      return first + " " + last
    else:
      return a
  arranged_authors = list(map(rearrange, authors))
  if len(arranged_authors) == 1:
    return arranged_authors[0]
  elif len(arranged_authors) == 2:
    return arranged_authors[0] + " and " + arranged_authors[1]
  else:
    res = ""
    for i in range(len(arranged_authors)-1):
      res += arranged_authors[i] + ", "
    res += "and " + arranged_authors[-1]
    return res

def get_title(e):
  if e[0] == "inbook" and "chapter" in e[2]:
    return e[2]["chapter"]
  else:
    return e[2]["title"] if "title" in e[2] else None

# format the name of the entry to authorYYYYtitle
def format_entry_name(e):
  title_str = get_title(e)
  if "author" not in e[2] or "year" not in e[2] or title_str is None:
    return e[1]  # failed to change it

  # get the last name of the first author, roughly
  authstr = e[2]["author"].replace("-","").replace("."," ")
  if "," in authstr:
    words = authstr[:authstr.index(",")].split()
    auth_part = simplify("".join(words))
  else:
    if "and" in authstr:
      authstr = authstr[:authstr.index("and")].strip()
    auth_part = simplify(authstr.split()[-1])

  # get the first "real" word of the title
  title_words = [simplify(s) for s in title_str.replace("-"," ").split()]
  i = 0
  while i < len(title_words) and title_words[i] in title_skipset:
    i += 1
  title_part = title_words[i]
  return auth_part + e[2]["year"] + title_part
  

# given an entry as a triple (entrytype, entryname, entrydict)
# and a list of lines written so far,
# append this entry in a standardized bibtex format
def append_bib_entry(e, lines):
  lines.append("@" + e[0] + "{" + e[1] + ",")
  # first all "preferred" fields in their order, then the other fields
  for field in preferred_fields_order:
    if field in e[2]:
      lines.append("  " + field + " = {" + e[2][field] + "},")
  for field in sorted(e[2]):
    if field not in preferred_fields_order:
      lines.append("  " + field + " = {" + e[2][field] + "},")
  lines.append("}")
      
# given an entry as a triple (entrytype, entryname, entrydict)
# return a string giving a plain-text version of an entry
def get_plain_entry(e, year_first=False):
  entry = e[2]
  place = ""
  if "series" in entry:
    place = entry["series"]
  elif "journal" in entry:
    place = entry["journal"]
  elif "booktitle" in entry:
    place = entry["booktitle"]
  elif e[0] == "phdthesis":
    place = "PhD thesis"
  if year_first:
    if len(place) > 0:
      place += "."
    return "(" + entry["year"] + ") " + format_author(entry["author"]) + ". " + entry["title"] + ". " + place
  else:
    if len(place) > 0:
      place += ", "
    return format_author(entry["author"]) + ". " + entry["title"] + ". " + place + entry["year"] + "."
 
    
# given filename and list of entry names
# return a list of triples: (entrytype, entryname, and entrydict)
def get_refs(filename, entrylist):
  results = []
  if not os.path.exists(filename):
    return results
  with open(filename) as f:
    while True:
      line = f.readline()
      if line == "":
        break
      line = line.strip()
      if "@" not in line or "{" not in line or "," not in line:
        continue
      entrytype = line[line.index("@")+1:line.index("{")].strip().lower()
      entryname = line[line.index("{")+1:line.index(",")].strip()
      if len(entrylist) > 0 and entryname not in entrylist:
        continue
      entry_dict = {}
      while True:
        line = f.readline()
        if line == "":
          break
        line = line.strip()
        if line == "}":
          break
        fields = line.split("=")
        if len(fields) != 2 or "{" not in fields[1] or "}" not in fields[1]:
          continue
        key = fields[0].strip().lower()
        val = fields[1][fields[1].index("{") + 1:fields[1].rfind("}")]
        entry_dict[key] = val
      results.append([entrytype, entryname, entry_dict])
  return results


if __name__ == "__main__":
  if len(sys.argv) < 2:
    print(usage_str)
    exit(0)
  args = sys.argv[1:]

  sort_by = "year"
  if "--author" in args:
    sort_by = "author"
    args.remove("--author")

  plain = False
  if "--plain" in args:
    plain = True
    args.remove("--plain")

  rename = False
  if "--rename" in args:
    rename = True
    args.remove("--rename")

  if len(args) == 0 or not os.path.exists(args[0]):
    print(usage_str)
    exit(0)

  bib = Bib()
  bib.readfile(args[0], args[1:])

  if plain:
    lines = bib.get_plain_lines(sort_by=sort_by)
  else:
    lines = bib.get_bib_lines(rename=rename, sort_by=sort_by)
  print("\n".join(lines))

