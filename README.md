# README #

This is a basic tool for very basic printing and formatting of LaTeX bibliography entries from .bib files.
Run "python3 printbib.py" with no arguments to see usage instructions.

Given a Bibtex .bib file, the tool can print out
  (1) all entries (default), or
  (2) only the entries named,
  
with entries printed in
  (a) bibtex format (default), or
  (b) plain text format,

sorted by
  (i) year followed by entry name (default), or
  (ii) author.
  
It can optionally attempt to re-name entries in a shortened format that looks like authorYYYYtitle.
  
  
Warning: the tool does not handle many special cases! It also expects bib entries to be formatted in the following style (whitespace within a line irrelevant):
@entrytype{entryname,
  field1 = {content1},
  field2 = {content2},
  ...
}